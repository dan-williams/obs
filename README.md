# README #

### What is this repository for? ###
* This repository is for a E-Commerce site for Ves written in java with a mariaDB database, and different java bases technologies.
* Version 0.0 (no full builds yet)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

To start set up we will need to install Java SDK and MariaDB.
First we will set up java as follows:

* download and install the JDK from (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Configure Java alternatives ( sudo alternatives --config java )
* Configure Javaws alternatives ( sudo alternatives --config javaws )
* Configure Javac alternatives ( sudo alternatives --config javac )
* Check java is installed (java -version).

Now to set up MariaDB:

* Config MariaDB repo ( sudo wget -O /etc/yum.repos.d/MariaDB.repo http://mariadb.if-not-true-then-false.com/fedora/$(rpm -E %fedora)/$(uname -i)/10_1 )
* Install MariaDB (sudo dnf install mariadb mariadb-server).
* Start MariaDB Service (sudo systemctl start mariadb.service).
* Enable MariaDB Service ( sudo systemctl enable mariadb.service )
* Set a root password (sudo /usr/bin/mysql_secure_installation) Follow the instructions you can say Yes to most of these.
* Run SQL script in repo to set up the app user on database ( mysql < SetUpDatabase.sql -u root -p )
* If you wish so you can download MySQL work bench or DataGrip.

### Contribution guidelines ###

* Write tests for all execution paths of functions you write.
* Write full stack tests.
* All tests must be passing before being merged.
* Code must be thoroughly reviewed before merge requests are to be accepted.
* Branches must be named after the related issue.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact