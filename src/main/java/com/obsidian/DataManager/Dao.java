package com.obsidian.DataManager;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.service.ServiceRegistry;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import java.util.List;

/**
 * Created by dan on 20/04/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Dao<TBo>{

    private static SessionFactory factory;
    private Class<TBo> persistentClassType;

    public Dao(Class<TBo> type){
        this.persistentClassType = type;
    }

    private static SessionFactory ConfigSession(){
        Logger.getLogger("org.hibernate").setLevel(Level.INFO);
        if (null==factory) {
            // loads configuration and mappings
            Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
            ServiceRegistry serviceRegistry
                    = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();

            // builds a session factory from the service registry
            factory = configuration.buildSessionFactory(serviceRegistry);
        }
        return factory;
    }

    public void AddEntity(TBo entity){
        Session session = ConfigSession().getCurrentSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.save(entity);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null && tx.isActive()) {
                tx.rollback();
            }
            throw e;
            //e.printStackTrace();
        }finally {
            if(session.isOpen()){
            session.close();
            }
        }
    }

    public TBo FindById(long id) {
        org.hibernate.Session session = ConfigSession().getCurrentSession();
        Transaction tx = null;
        TBo entity = null;

        try{
            tx = session.beginTransaction();
            entity = (TBo) session.get(persistentClassType, id);

            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null && tx.isActive()) {
                tx.rollback();
            }
            //e.printStackTrace();
        }finally {
            if(session.isOpen()){
                session.close();
            }
        }

        return entity;
    }

    public List<TBo> FindAll(Criterion... criterion) {

        org.hibernate.Session session = ConfigSession().getCurrentSession();
        session.beginTransaction();
        Criteria crit = session.createCriteria(persistentClassType);

        for (Criterion c : criterion) {
            crit.add(c);
        }
        List tmp=crit.list();
        session.getTransaction().commit();
        return tmp;
    }

    public List<TBo> FindByExample(TBo exampleInstance, String[] excludeProperty) {
        org.hibernate.Session session = ConfigSession().getCurrentSession();
        session.beginTransaction();
        Criteria crit = session.createCriteria(persistentClassType);

        Example example =  Example.create(exampleInstance);
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        crit.add(example);
        return crit.list();
    }

    public void UpdateEntity(TBo entity){
        org.hibernate.Session session = ConfigSession().getCurrentSession();
        Transaction tx = session.beginTransaction();
        try{
            session.update(entity);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null && tx.isActive()) {
                tx.rollback();
            }
           // e.printStackTrace();
        }finally {
            if (session.isOpen()){
            session.close();
            }
        }
    }

    public void DeleteEntity(TBo entity){
        org.hibernate.Session session = ConfigSession().getCurrentSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();

            session.delete(entity);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null && tx.isActive()) {
                tx.rollback();
            }
            //e.printStackTrace();
        }finally {
            if(session.isOpen()){
            session.close();
            }
        }
    }
}
