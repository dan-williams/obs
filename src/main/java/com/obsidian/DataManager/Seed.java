package com.obsidian.DataManager;

/**
 * Created by dan on 20/04/16.
 */
import com.obsidian.Model.Necklace;

public class Seed{

    public static void main(String[] args) {
        Seed seed = new Seed();

        seed.SeedNecklaces();
    }

    private void SeedNecklaces() {
        System.out.println("Populating Necklaces...");

        Dao dao = new Dao<Necklace>(Necklace.class);

        AddNecklace(dao, "Emerald Queen", "4mm", 100, "Emerald Jewel");
        AddNecklace(dao, "Perl joob", "F", 50, "Perl Necklace");

        System.out.println("Complete!");
    }

    private void AddNecklace(Dao dao, String name, String size, int price, String description) {
        System.out.println(String.format("Creating Neckalce: %s", name));
        Necklace necklace = new Necklace(name, size, price, description);

        dao.AddEntity(necklace);
        System.out.println(String.format("Created %s", name));
    }
}

