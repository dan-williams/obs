package com.obsidian.Model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by dan on 20/04/16.
 */
@MappedSuperclass
public abstract class AbstractBo {

    @Id
    @GeneratedValue
    @Column(name = "Id")
    private long _id;

    public long get_id(){
        return _id;
    }

    public void set_id(long id){
        _id = id;
    }
}
