package com.obsidian.Model;

import javax.persistence.*;

/**
 * Created by dan on 20/04/16.
 */

@Entity
@Table(name = "Necklace")
public class Necklace extends AbstractBo {

    @Column(name = "Name")
    private String _name;

    @Column(name = "Size")
    private String _size;

    @Column(name = "Price")
    private int _price;

    @Column(name = "Description")
    private String _description;

    public Necklace(){}
    public Necklace(String name, String size, int price, String desc){
        _name = name;
        _size = size;
        _price = price;
        _description = desc;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_size() {
        return _size;
    }

    public void set_size(String _size) {
        this._size = _size;
    }

    public int get_price() {
        return _price;
    }

    public void set_price(int _price) {
        this._price = _price;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_description() {
        return _description;
    }
}
