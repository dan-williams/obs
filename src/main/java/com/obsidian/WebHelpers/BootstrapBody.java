package com.obsidian.WebHelpers;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

/**
 * Created by dan on 16/05/16.
 */

public class BootstrapBody extends SimpleTagSupport {

    private String title;

    public void setTitle(String text) {
        this.title = text;
    }

    StringWriter sw = new StringWriter();

    public void doTag()
            throws JspException, IOException
    {
        if (title != null) {
          /* Use message from attribute */
            JspWriter out = getJspContext().getOut();
            String navBar = "<nav class=\"navbar navbar-inverse\">\n" +
                    "  <div class=\"container-fluid\">\n" +
                    "    <div class=\"navbar-header\">\n" +
                    "      <a class=\"navbar-brand\" href=\"/Home.jsp\">Obsidian\n" +
                    "      <img class=\"img-responsive\" src=\"placeholder.jpg\">\n" +
                    "      </a>\n" +
                    "    </div>\n" +
                    "    <div>\n" +
                    "      <ul class=\"nav navbar-nav\">\n" +
                    "        <li class=\"active\"><a href=\"/Home.jsp\">Home</a></li>\n" +
                    "        <li class=\"dropdown\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Jewellery<span class=\"caret\"></span></a>\n" +
                    "          <ul class=\"dropdown-menu\">\n" +
                    "            <li><a href=\"/Necklaces.jsp\">Necklaces</a></li>\n" +
                    "          </ul>\n" +
                    "        </li>\n" +
                    "        <li class=\"dropdown\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">Clothing<span class=\"caret\"></span></a>\n" +
                    "          <ul class=\"dropdown-menu\">\n" +
                    "            <li><a href=\"/Tops.jsp\">Tops</a></li>\n" +
                    "          </ul>\n" +
                    "        </li>\n" +
                    "      </ul>\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "</nav>";
            String head = "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n" +
                    "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                    "<style type=\"text/css\">\n" +
                    "    <%@include file=\"bootstrap/css/bootstrap.min.css\" %>\n" +
                    "    <%@include file=\"bootstrap/css/business-casual.css\"%>\n" +
                    "</style>\n" +
                    "\n" +
                    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                    "<script src=\"bootstrap/js/jquery.js\"></script>\n" +
                    "<script src=\"bootstrap/js/bootstrap.js\"></script>\n" +
                    "<title>"+ title +"</title>\n" +
                    "</head><body>\n" +
                    "<div class=\"container-fluid\">\n";
            getJspBody().invoke(sw);

            out.println(head + navBar + sw.toString() + "</div>\n" +
                    "</body>\n" +
                    "</html>");
        }
    }
}
