package com.obsidian.WebHelpers;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Created by dan on 16/05/16.
 */
public class Button extends SimpleTagSupport {
    private String value;

    public void setValue(String text) {
        this.value = text;
    }


    public void doTag()
            throws JspException, IOException
    {
        JspWriter out = getJspContext().getOut();

        String button = String.format("<button type=\"button\" class=\"btn btn-primary btn-md\">%s</button>", value);

        out.println(button);
    }
}
