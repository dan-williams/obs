package com.obsidian.WebHelpers;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Created by dan on 16/05/16.
 */
public class SubmitButton extends SimpleTagSupport {
    private String value;

    public void setValue(String value) {
        this.value = value;
    }


    public void doTag()
            throws JspException, IOException
    {
        JspWriter out = getJspContext().getOut();

        String button = String.format("<input type=\"submit\" class=\"btn btn-info\" value=%s>", value);

        out.println(button);
    }
}
