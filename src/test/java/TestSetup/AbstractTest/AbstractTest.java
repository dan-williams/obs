package TestSetup.AbstractTest;

import com.google.gson.GsonBuilder;

import static org.junit.Assert.assertEquals;

/**
 * Created by dan on 26/04/16.
 */
public abstract class AbstractTest extends TestBuilders{

    private String prettyPrint(Object entity){
        return new GsonBuilder().setPrettyPrinting().create().toJson(entity);
    }

    public void AssertIsEqual(Object expected, Object actual){

        String expectedAsJson = prettyPrint(expected);
        String actualAsJson = prettyPrint(actual);

        assertEquals(expectedAsJson, actualAsJson);

        System.out.println(String.format("expected: %s", expectedAsJson));
        System.out.println(String.format("actual: %s", actualAsJson));
    }

}
