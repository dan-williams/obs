package TestSetup.Builders;

import com.obsidian.DataManager.Context;

/**
 * Created by dan on 26/04/16.
 */
public interface  AbstractBuilder<TBo> {
    public TBo Build();

    public TBo BuildAndPersist(Context context);

    public TBo BuildAndPersistWithId(Context context, long id);

    public TBo BuildWithId(long id);
}
