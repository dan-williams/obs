package TestSetup.Builders;

import com.obsidian.DataManager.Context;
import com.obsidian.Model.Necklace;

/**
 * Created by dan on 26/04/16.
 */
public class NecklaceBuilder implements AbstractBuilder<Necklace> {
    private String _name;
    private String _size;
    private int _price;
    private String _description;

    @Override
    public Necklace Build() {
        return new Necklace(_name, _size, _price, _description);
    }

    @Override
    public Necklace BuildAndPersist(Context context) {
        Necklace necklace = Build();
        context.Necklace.AddEntity(necklace);
        return necklace;
    }

    @Override
    public Necklace BuildAndPersistWithId(Context context, long id) {
        Necklace necklace = BuildWithId(id);
        context.Necklace.AddEntity(necklace);
        return necklace;
    }

    @Override
    public Necklace BuildWithId(long id) {
        Necklace necklace = Build();
        necklace.set_id(id);
        return necklace;
    }

    public NecklaceBuilder SetName(String name) {
        _name = name;
        return this;
    }

    public NecklaceBuilder SetSize(String size) {
        _size = size;
        return this;
    }

    public NecklaceBuilder SetPrice(int price) {
        _price = price;
        return this;
    }

    public NecklaceBuilder SetDescription(String description) {
        _description = description;
        return this;
    }
}
