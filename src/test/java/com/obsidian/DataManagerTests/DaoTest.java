package com.obsidian.DataManagerTests;

import TestSetup.AbstractTest.AbstractIntegrationTest;
import com.google.gson.GsonBuilder;
import com.obsidian.DataManager.Context;
import com.obsidian.Model.Necklace;
import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dan on 30/04/16.
 */
public class DaoTest extends AbstractIntegrationTest {

    @Test
    public void AddEntity_Success(){

        Necklace expected = NewNecklace()
                .Build();

        Context context = NewContext();

        context.Necklace.AddEntity(expected);

        Necklace actual = context.Necklace.FindById(expected.get_id());

        AssertIsEqual(expected, actual);
    }

    @Test
    public void FindById_Success(){

        Context context = NewContext();

        Necklace expected = NewNecklace()
                .BuildAndPersist(context);

        Necklace actual = context.Necklace.FindById(expected.get_id());

        AssertIsEqual(expected, actual);
    }

    @Test
    public void FindAll_Success(){

        Context context = NewContext();

        Necklace necklace1 = NewNecklace()
                .SetName("necklace1")
                .BuildAndPersist(context);
        Necklace necklace2 = NewNecklace()
                .SetName("necklace2")
                .BuildAndPersist(context);
        Necklace necklace3 = NewNecklace()
                .SetName("necklace3")
                .BuildAndPersist(context);
        Necklace necklace4 = NewNecklace()
                .SetName("necklace4")
                .BuildAndPersist(context);

        List<Necklace> expected = new ArrayList<>();

        expected.add(necklace1);
        expected.add(necklace2);
        expected.add(necklace3);
        expected.add(necklace4);


        List<Necklace> actual = context.Necklace.FindAll();
        AssertIsEqual(expected, actual);
    }

    @Test
    public void UpdateEntity_Success() {

        Context context = NewContext();

        Necklace saved = NewNecklace()
                .SetName("Original Necklace")
                .SetPrice(45)
                .SetSize("mm")
                .BuildAndPersist(context);

        Necklace expected = NewNecklace()
                .SetName("expected Necklace")
                .SetPrice(25)
                .SetSize("oo")
                .BuildWithId(saved.get_id());

        context.Necklace.UpdateEntity(expected);

        Necklace actual = context.Necklace.FindById(expected.get_id());

        AssertIsEqual(expected, actual);
    }

    @Test
    public void DeleteEntity_Success(){

        Context context = NewContext();

        Necklace necklace = NewNecklace()
                .BuildAndPersist(context);

        context.Necklace.DeleteEntity(necklace);

        assertFalse(NecklaceIsInDb(context, necklace));
    }

    private boolean NecklaceIsInDb(Context context, Necklace necklace){

        Necklace necklaceInDb = context.Necklace.FindById(necklace.get_id());
        return necklaceInDb == necklace;
    }
}
