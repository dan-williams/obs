package com.obsidian.DataManagerTests;

import TestSetup.AbstractTest.AbstractIntegrationTest;
import com.obsidian.DataManager.Context;
import com.obsidian.DataManager.Seed;
import com.obsidian.Model.Necklace;
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dan on 30/04/16.
 */
public class SeedTest extends AbstractIntegrationTest {

    @Test
    public void Seed_Success(){

        Necklace expectedNecklace1 = NewNecklace()
                .SetName("Emerald Queen")
                .SetDescription("Emerald Jewel")
                .SetSize("4mm")
                .SetPrice(100)
                .Build();

        Necklace expectedNecklace2 = NewNecklace()
                .SetName("Perl joob")
                .SetSize("F")
                .SetPrice(50)
                .SetDescription("Perl Necklace")
                .Build();

        Context context = NewContext();

        Seed.main(new String[]{});

        List<Necklace> actualNecklaceList = context.Necklace.FindByExample(expectedNecklace2, new String[]{});
        System.out.println(Arrays.toString(actualNecklaceList.toArray()));
        AssertIsEqual(expectedNecklace1, actualNecklaceList.get(0));
        AssertIsEqual(expectedNecklace2, actualNecklaceList.get(1));
    }
}
